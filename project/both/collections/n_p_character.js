NPCharacter = new Mongo.Collection('n_p_character');

NPCharacter.attachSchema(new SimpleSchema({

  title:{
    type:String
  },
  npCharacterRequestIds:{
    type:[String],
    optional: true
  },
  npCharacterDescription:{
    type:String
  },
  item:{
    type:String
  }

}));