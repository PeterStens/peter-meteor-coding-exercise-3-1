/*****************************************************************************/
/* Playerresponse: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.Playerresponse.events({
  /*
   * Example:
   *  'click .selector': function (e, tmpl) {
   *
   *  }
   */
});

Template.Playerresponse.helpers({
  responsefinder: function () {
    return PlayerResponse.find({});
  }
});

/*****************************************************************************/
/* Playerresponse: Lifecycle Hooks */
/*****************************************************************************/
Template.Playerresponse.created = function () {
};

Template.Playerresponse.rendered = function () {
};

Template.Playerresponse.destroyed = function () {
};