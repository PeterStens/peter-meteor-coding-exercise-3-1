/*****************************************************************************/
/* Npcharacterrequest: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.Npcharacterrequest.events({
  /*
   * Example:
   *  'click .selector': function (e, tmpl) {
   *
   *  }
   */
});

Template.Npcharacterrequest.helpers({
 npcrequestfinder: function () {
    return NPCharacterRequest.find({});
 }
});

/*****************************************************************************/
/* Npcharacterrequest: Lifecycle Hooks */
/*****************************************************************************/
Template.Npcharacterrequest.created = function () {
};

Template.Npcharacterrequest.rendered = function () {
};

Template.Npcharacterrequest.destroyed = function () {
};