/*****************************************************************************/
/* Playerrequest: Event Handlers and Helpersss .js*/
/*****************************************************************************/
Template.Playerrequest.events({
  /*
   * Example:
   *  'click .selector': function (e, tmpl) {
   *
   *  }
   */
});

Template.Playerrequest.helpers({
  requestfinder: function () {
    return PlayerRequest.find({});
  }
});

/*****************************************************************************/
/* Playerrequest: Lifecycle Hooks */
/*****************************************************************************/
Template.Playerrequest.created = function () {
};

Template.Playerrequest.rendered = function () {
};

Template.Playerrequest.destroyed = function () {
};